import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { RouterModule } from '@angular/router';
import { AuthModule, OidcConfigService } from 'angular-auth-oidc-client';
import { AuthInterceptor } from './core/interceptors/auth.interceptor';
import { ConfigService } from './core/services';
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

export function initConfigAndAuth(config: ConfigService, oidcConfigService: OidcConfigService) {
  return () => config.loadConfigs('./assets/env/environment.json').then(
    () => {
      const configs = config.getConfigsList();
      oidcConfigService.withConfig({
        stsServer: configs.stsServer,
        redirectUrl: configs.redirectUrl,
        postLogoutRedirectUri: window.location.origin,
        clientId: configs.clientId,
        scope: configs.scope,
        responseType: configs.responseType,
        silentRenew: configs.silentRenew,
        useRefreshToken: configs.useRefreshToken,
        // logLevel: configs.isDevelopment ? LogLevel.Debug : LogLevel.None
      })
    });
}

@NgModule({
  imports: [BrowserModule,
    RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    AuthModule.forRoot(),
    HttpClientModule
  ],
  declarations: [AppComponent, UnauthorizedComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initConfigAndAuth,
      deps: [ConfigService, OidcConfigService],
      multi: true,
    }
  ],
  bootstrap: [AppComponent],
})


export class AppModule { }

