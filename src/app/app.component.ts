import { Component, OnInit } from '@angular/core';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { AuthService } from './core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'acme-bidding-presentation';

  constructor(public oidcSecurityService: OidcSecurityService, private authService: AuthService) { }

  ngOnInit(): void {
    this.oidcSecurityService
      .checkAuth().subscribe((isAuthenticated) => {
        this.authService.isAuthenticated.next(isAuthenticated);
      });
  }
}
