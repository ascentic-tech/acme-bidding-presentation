import { Paging } from './paging';
export interface AppResponse<T> {
  data?: T;
  isSuccess?: boolean
  message?: string
  paging?: Paging;
}
