import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ConfigService } from './config.service';

@Injectable()
export class PermissionService {
  private userPermissions: Array<string> = new Array();
  private staticPermissions: any = new Array();
  private loaded = false;

  constructor(
    private apiService: ApiService,
    private configService: ConfigService,
    private httpClient: HttpClient
  ) { }

  getPath(): string {
    return this.configService.getConfigsList().baseAPIurl;
  }

  loadStaticPermissions(url: string): Promise<any> {
    let rt: Promise<any>;
    if (!this.loaded) {
      const promise = this.httpClient.get(url).toPromise();

      promise.then(permissions => {
        this.staticPermissions = permissions;
        this.loaded = true;
      });
      rt = promise;
    }

    return rt;
  }

  getStaticPermission(permissionCode: string): any {
    const propVal = this.staticPermissions[permissionCode];
    if (propVal) {
      return propVal;
    }
    alert('NO_PERMISSION_FOUND!. Please provide valid permission code.');
    return null;
  }

  loadUserPermissions(userName: string): Promise<any> {
    let rt: Promise<any>;
    if (!this.loaded) {

      const url = `${this.getPath()}/User/Search`;
      let params: HttpParams = new HttpParams();

      if (userName) {
        params = params.set('username', userName.toString());
      }
      const promise = this.apiService.get<Array<string>>(url, params).toPromise();
      promise.then(permissions => {
        if (permissions.isSuccess) {
          this.userPermissions = permissions.data;
          this.loaded = true;
        } else {

        }
      });
      rt = promise;
    }
    return rt;
  }

  checkUserPermission(permissionCode: string): boolean {
    return true;
    // const realPermissionCode = this.getStaticPermission(permissionCode);
    // return realPermissionCode ? this.userPermissions.some(permission => permission === realPermissionCode) : false;
  }

  getUserPersmissions() {
    return this.userPermissions;
  }
}
