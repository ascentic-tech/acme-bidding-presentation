import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SignalRService {
  public latestBid = new BehaviorSubject<any>(null);
  private hubConnection: HubConnection
  constructor() { }

  public startConnection() {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('https://localhost:44340/acmeSignalRHub')
      .build();

    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started')
      })
      .catch(err => {
        console.log('Error while starting connection: ' + err)
      })
  }

  public addTransferLatestBidListener() {
    this.hubConnection.on('BroadcastItemData', (response) => {
      const data = JSON.parse(response);
      this.latestBid.next(data);
    });
  }
}

