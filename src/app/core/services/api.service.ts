import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BroadcastService } from './broadcast.service';
import { AppResponse } from 'src/app/core/models/response-models/app-response';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(
    private httpClient: HttpClient,
    private broadCastService: BroadcastService,
  ) { }

  get<T>(path: string, params?: HttpParams): Observable<AppResponse<T>> {
    return this.httpClient
      .get<AppResponse<T>>(path, { params })
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.handleError(err);
          return of<AppResponse<T>>({ isSuccess: err.error.isSuccess, message: err.error.message });
        })
      );
  }

  getFile(path: string, params: HttpParams = new HttpParams()): any {
    return this.httpClient
      .get(path, { responseType: 'blob', params })
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.handleError(err);
          return of<any>({ isSuccess: err.error.isSuccess, message: err.error.message });
        })
      );
  }

  put<T>(path: string, body: Object = {}): Observable<AppResponse<T>> {
    return this.httpClient
      .put<AppResponse<T>>(path, body)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.handleError(err);
          return of<AppResponse<T>>({ isSuccess: err.error.isSuccess, message: err.error.message });
        })
      );
  }

  post<T>(path: string, body: Object = {}): Observable<AppResponse<T>> {
    return this.httpClient
      .post<AppResponse<T>>(path, body)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.handleError(err);
          return of<AppResponse<T>>({ isSuccess: err.error.isSuccess, message: err.error.message });
        })
      );
  }

  delete<T>(path): Observable<AppResponse<T>> {
    return this.httpClient
      .delete<AppResponse<T>>(path)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          this.handleError(err);
          return of<AppResponse<T>>({ isSuccess: err.error.isSuccess, message: err.error.message });
        })
      );
  }

  private handleError(error: HttpErrorResponse): void {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      this.broadCastService.broadcast('CLIENT_SIDE_NETWORK_ERROR', error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      this.broadCastService.broadcast('SERVER_ERROR', error);
    }
  }
}
