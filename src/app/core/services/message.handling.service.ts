import { Injectable } from '@angular/core';
import { MessageService, Message } from 'primeng/api';

@Injectable({
  providedIn: 'root',
})
export class MessageHandlingService {
  constructor(private msgService: MessageService) { }

  public handleErrorMessage(
    summary: string = 'Error',
    detail: string
  ): void {
    const msg: Message = {
      key: 'tr',
      severity: 'error',
      summary,
      detail,
      closable: false
    };
    this.publishMessage(msg);
  }

  public handleSuccessMessage(
    summary: string = 'Success',
    detail: string
  ): void {
    const msg: Message = {
      key: 'tr',
      severity: 'success',
      summary,
      detail
    };
    this.publishMessage(msg);
  }

  public handleConfirmationMessage(
    summary: string = 'Success',
    detail: string,
    data: any
  ): void {
    const msg: Message = {
      key: 'c',
      sticky: true,
      closable: false,
      severity: 'warn',
      summary,
      detail,
      data
    };
    this.msgService.clear();
    this.publishMessage(msg);
  }



  public handleNotificationMessage(
    notifyMsg: string,
    title: string = 'Notification'
  ): void {
    const msg: Message = {
      severity: 'info',
      summary: title,
      detail: notifyMsg
    };
    this.publishMessage(msg);
  }

  private publishMessage(msg: Message): void {
    this.msgService.add(msg);
  }
}
