import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  private configs: any = new Array();
  private loaded = false;

  constructor(
    private httpClient: HttpClient
  ) { }

  loadConfigs(url: string): Promise<any> {
    let rt: Promise<any>;
    if (!this.loaded) {
      const promise = this.httpClient.get(url).toPromise();

      promise.then(configs => {
        this.configs = configs[environment.CURRENT_MODE];
        this.loaded = true;
      });
      rt = promise;
    }

    return rt;
  }

  getConfig(configName: string): any {
    const propVal = this.configs[configName];
    if (propVal) {
      return propVal;
    }
    alert('NO_CONFIG_FOUND!. Please provide valid config name.');
    return null;
  }

  getConfigsList() {
    return this.configs;
  }
}
