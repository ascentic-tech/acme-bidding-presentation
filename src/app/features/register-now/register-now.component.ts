import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { UserRegistration } from '../shared/models';
import { UserService } from '../shared/services';

@Component({
  selector: 'app-register-now',
  templateUrl: './register-now.component.html',
  styleUrls: ['./register-now.component.scss']
})

export class RegisterNowComponent implements OnInit {
  userRegistration: UserRegistration = {};

  constructor(public oidcSecurityService: OidcSecurityService,
    public userService: UserService,
    private router: Router) { }


  ngOnInit(): void {

  }

  onRegister() {
    this.userService.createUser(this.userRegistration).subscribe(response => {
      if (response.isSuccess) {
         this.router.navigate(['/']);
      }
    });
  }


  login() {
    this.oidcSecurityService.authorize();
  }

  resedPIN() {

  }
}
