import { Component, OnInit } from '@angular/core';
import { LazyLoadEvent, SelectItem } from 'primeng/api';
import { AuthService, SignalRService } from 'src/app/core/services';
import { Item, ItemBid } from '../../shared/models';
import { ItemBidService, ItemService } from '../../shared/services';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss']
})
export class ListItemsComponent implements OnInit {
  isAuthenticated = false;
  virtualItems: Item[] = [];

  sortOptions: SelectItem[];
  sortKey: string;
  sortOrder: number;
  sortField: string;
  totalRecords: number;
  itemStatuses = [
    'None',
    'New',
    'Active',
    'Sold',
    'Scheduled',
    'Unsold',
    'Deleted'
  ]


  isBidAddingVisible = false;
  selectedBiddingItem: Item = {};
  itemBid: ItemBid = {};

  constructor(private itemService: ItemService,
    private itemBidService: ItemBidService,
    private authService: AuthService,
    private signalRService: SignalRService) {
  }

  ngOnInit(): void {
    this.isAuthenticated = this.authService.isAuthenticated.value;
    this.authService.isAuthenticated.subscribe(isLoggedInEvent => {
      this.isAuthenticated = isLoggedInEvent;
    })


    this.virtualItems = Array.from({ length: 10000 });

    this.signalRService.startConnection();
    this.signalRService.addTransferLatestBidListener();

    this.sortOptions = [
      { label: 'Latest Bid High to Low', value: '!bid' },
      { label: 'Latest Bid Low to High', value: 'bid' }
    ];

    this.signalRService.latestBid.subscribe(data => {
      if (data) {
        this.virtualItems?.forEach((item, index) => {
          const obj =JSON.parse(data);
          if (item.id === obj.itemId) {
            this.virtualItems[index].highestBid = obj.bidAmount;
          }
        })
      }
    });
  }

  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    }
    else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  loadLazy(event: LazyLoadEvent) {
    this.itemService.getItemListByStatus(2, (event.rows / 10) - 1, event.rows).subscribe(response => {
      if (response.isSuccess) {
        this.virtualItems = response.data;
        this.totalRecords = response.paging.totalRecords;
      }
    });
  }

  onBidAddingVisible(item: Item) {
    this.selectedBiddingItem = item;
    this.isBidAddingVisible = true;
  }

  onSaveBid() {
    this.itemBid.itemId = this.selectedBiddingItem.id;
    this.itemBidService.addItemBid(this.itemBid).subscribe(response => {
      if (response.isSuccess) {
        this.isBidAddingVisible = false;
      }
    });
  }
}
