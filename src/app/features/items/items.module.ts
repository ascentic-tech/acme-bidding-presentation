import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ItemsRoutingModule } from './items-routing.module';
import { ListItemsComponent } from './list-items/list-items.component';
import { ViewItemComponent } from './view-item/view-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { AddItemComponent } from './add-item/add-item.component';
import { AddBidComponent } from './add-bid/add-bid.component';

@NgModule({
  declarations: [ListItemsComponent, ViewItemComponent, EditItemComponent, AddItemComponent, AddBidComponent],
  imports: [
    SharedModule,
    ItemsRoutingModule
  ]
})
export class ItemsModule { }
