import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListItemsComponent } from './list-items/list-items.component';
import { AddItemComponent } from './add-item/add-item.component';
import { ViewItemComponent } from './view-item/view-item.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { AuthGuard } from 'src/app/core/services';
import { AddBidComponent } from './add-bid/add-bid.component';

const routes: Routes = [
  {
    path: '',
    component: ListItemsComponent
  },
  {
    path: 'add',
    component: AddItemComponent
  },
  {
    path: ':id/view',
    component: ViewItemComponent
  },
  {
    path: ':id/edit',
    component: EditItemComponent
  },
  {
    path: ':id/bid/add',
    component: AddBidComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ItemsRoutingModule { }

