import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppResponse } from 'src/app/core/models';
import { ApiService, ConfigService } from 'src/app/core/services';
import { User, UserRegistration } from '../models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private apiService: ApiService,
    private configService: ConfigService
  ) { }

  getPath(): string {
    return this.configService.getConfigsList().baseAPIurl;
  }

  public createUser(userRegistration: UserRegistration): Observable<AppResponse<User>> {
    const url = `${this.getPath()}/api/User/AddUserAsync`;
    return this.apiService.post(url, userRegistration);
  }
}
