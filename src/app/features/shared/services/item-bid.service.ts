import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppResponse } from 'src/app/core/models';
import { ApiService, ConfigService } from 'src/app/core/services';
import { ItemBid } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ItemBidService {

  constructor(
    private apiService: ApiService,
    private configService: ConfigService
  ) { }

  getPath(): string {
    return this.configService.getConfigsList().baseAPIurl;
  }

  public addItemBid(bid: ItemBid): Observable<AppResponse<ItemBid>> {
    const url = `${this.getPath()}/api/ItemBid/AddItemBidAsync`;
    return this.apiService.post<ItemBid>(url, bid);
  }

}
