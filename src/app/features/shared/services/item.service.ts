import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppResponse } from 'src/app/core/models';
import { ApiService, ConfigService } from 'src/app/core/services';
import { Item } from '../models';

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  constructor(
    private apiService: ApiService,
    private configService: ConfigService
  ) { }

  getPath(): string {
    return this.configService.getConfigsList().baseAPIurl;
  }

  public getItemByIdAsync(id: number): Observable<AppResponse<Item>> {
    const url = `${this.getPath()}/api/Item/GetItemByIdAsync/${id}`;
    return this.apiService.get<Item>(url);
  }

  public getAllItemList(page?: number, pageSize?: number): Observable<AppResponse<Item[]>> {
    const url = `${this.getPath()}/api/Item/GetAllItemListAsync`;
    let params: HttpParams = new HttpParams();

    if (page) {
      params = params.set('page', page.toString());
    }

    if (pageSize) {
      params = params.set('pageSize', pageSize.toString());
    }

    return this.apiService.get<Item[]>(url, params);
  }

  public getItemListByStatus(itemStatus: number, page?: number, pageSize?: number): Observable<AppResponse<Item[]>> {
    const url = `${this.getPath()}/api/Item/GetItemListByStatusAsync`;
    let params: HttpParams = new HttpParams();

    if (itemStatus) {
      params = params.set('itemStatus', itemStatus.toString());
    }

    if (page) {
      params = params.set('page', page.toString());
    }

    if (pageSize) {
      params = params.set('pageSize', pageSize.toString());
    }

    return this.apiService.get<Item[]>(url, params);
  }

}
