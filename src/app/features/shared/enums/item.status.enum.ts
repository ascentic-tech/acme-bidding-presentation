enum ItemStatus {
  New = 1,
  Active = 2,
  Sold = 3,
  Scheduled = 4,
  Unsold = 5,
  Deleted = 6
}
