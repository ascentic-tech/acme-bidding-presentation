import { RolePermission } from "./role.permission.model";
import { UserRole } from "./user.role.model";


export interface Role {
  id?: number;
  name?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  userRoles?: UserRole[];
  rolePermissions?: RolePermission[];
}
