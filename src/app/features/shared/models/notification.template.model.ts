import { NotificationType } from "./notification.type.model";
import { Notification } from "./notification.model";


export interface NotificationTemplate {
  id?: number;
  title?: string;
  body?: string;
  notificationTypeId?: number;
  notificationType?: NotificationType;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  notifications?: Notification[];
}
