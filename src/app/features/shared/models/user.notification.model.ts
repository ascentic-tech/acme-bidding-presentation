import { Notification } from "./notification.model";


export interface UserNotification {
  id?: number;
  userId?: number;
  notificationId?: number;
  notification?: Notification;
  viewedAt?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
}
