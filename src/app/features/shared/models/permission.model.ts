import { RolePermission } from "./role.permission.model";

export interface Permission {
  id?: number;
  name?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  rolePermissions?: RolePermission[];
}
