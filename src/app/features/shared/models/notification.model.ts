import { NotificationTemplate } from "./notification.template.model";
import { UserNotification } from "./user.notification.model";


export interface Notification {
  id?: number;
  notificationContent?: string;
  notificationTemplateId?: number;
  notificationTemplate?: NotificationTemplate;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  userNotifications?: UserNotification[];
}
