import { User } from "./user.model";

export interface ItemBid {
  id?: number;
  userId?: number;
  user?: User;
  itemId?: number;
  bidAmount?: number;
  biddedAt?: string;
  biddedBy?: number;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
}
