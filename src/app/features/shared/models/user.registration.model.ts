import { Role } from "./role.model";


export interface UserRegistration {
  identityUserId?: string;
  firstName?: string;
  lastName?: string;
  userName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  phoneNumber?: string;
  redirectUrl?: string;
}
