
export interface ItemImage {
  id?: number;
  itemId?: number;
  url?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
}
