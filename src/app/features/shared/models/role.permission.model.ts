import { Permission } from "./permission.model";


export interface RolePermission {
  id?: number;
  permissionId?: number;
  permission?: Permission;
  roleId?: number;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
}
