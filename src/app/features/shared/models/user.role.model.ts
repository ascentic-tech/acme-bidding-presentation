import { Role } from "./role.model";


export interface UserRole {
  id?: number;
  userId?: number;
  roleId?: number;
  role?: Role;
  isActive?: boolean;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
}
