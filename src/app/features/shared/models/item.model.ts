import { ItemBid } from "./Item.bid.model";
import { ItemImage } from "./Item.Image.model";

export interface Item {
  id?: number
  title?: string
  description?: string
  startingBid?: number
  highestBid?: number
  startingTime?: string
  duration?: Date;
  itemStatus?: number
  itemBids?: ItemBid[]
  itemImages?: ItemImage[]
  createdAt?: string
  createdBy?: number
  lastModifiedAt?: string
  lastModifiedBy?: number
}



