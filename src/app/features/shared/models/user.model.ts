import { UserRole } from "./user.role.model";
import { UserNotification } from "./user.notification.model";
import { ItemBid } from "./Item.bid.model";


export interface User {
  id?: number;
  identityUserId?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  userNotifications?: UserNotification[];
  userRoles?: UserRole[];
  itemBids?: ItemBid[];
}
