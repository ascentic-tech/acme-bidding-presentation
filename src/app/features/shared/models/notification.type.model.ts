import { NotificationTemplate } from "./notification.template.model";

export interface NotificationType {
  id?: number;
  name?: string;
  description?: string;
  createdAt?: string;
  createdBy?: number;
  lastModifiedAt?: string;
  lastModifiedBy?: number;
  notificationTemplates?: NotificationTemplate[];
}
