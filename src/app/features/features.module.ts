import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FeaturesRoutingModule } from './features-routing.module';
import { FeaturesComponent } from './features.component';
import { RegisterNowComponent } from './register-now/register-now.component';


@NgModule({
  declarations: [
    FeaturesComponent, RegisterNowComponent],
  imports: [
    SharedModule,
    FeaturesRoutingModule
  ]
})
export class FeaturesModule { }
