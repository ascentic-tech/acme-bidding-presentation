import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/core/services';

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss']
})
export class TopBarComponent implements OnInit {
  items: MenuItem[];
  profileMenuItems: MenuItem[];
  isAuthenticated = false;
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticated.value;
    this.authService.isAuthenticated.subscribe(isAuthenticated => {
      this.isAuthenticated = isAuthenticated;
      this.items = [
        {
          label: 'Home',
          icon: 'pi pi-fw pi-home',
          visible: !this.isAuthenticated
        },
        {
          label: 'Home',
          icon: 'pi pi-fw pi-home',
          visible: this.isAuthenticated,
          items: [
            {
              label: 'Edit Auction',
              icon: 'pi pi-fw pi-pencil'
            }
          ]
        },
        {
          visible: this.isAuthenticated,
          label: 'Items',
          icon: 'pi pi-fw pi-list',
        },
        {
          visible: !this.isAuthenticated,
          label: 'Register',
          icon: 'pi pi-fw pi-user-plus',
          routerLink: ['/register-now']
        },
        {
          visible: !this.isAuthenticated,
          label: 'Sign In',
          icon: 'pi pi-fw pi-sign-in',
          command: () => this.login()
        }
      ];


      this.profileMenuItems = [
        {
          visible: this.isAuthenticated,
          label: 'Profile',
          icon: 'pi pi-fw pi-user',
          items: [
            {
              label: 'Edit',
              icon: 'pi pi-fw pi-user-edit'
            },
            {
              separator: true
            },
            {
              label: 'Sign out',
              icon: 'pi pi-fw pi-sign-out',
              command: () => this.logout()
            }
          ]
        }
      ];
    })
  }

  login() {
    this.authService.doLogin();
  }

  logout() {
    this.authService.isAuthenticated.next(false);
    this.authService.signOut();
  }

}
