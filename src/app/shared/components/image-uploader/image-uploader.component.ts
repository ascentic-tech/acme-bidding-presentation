import { Component, OnInit, ViewChild } from '@angular/core';
import { CropperPosition, Dimensions, ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {
  @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;
  public showImageCropperPopup: boolean = false;
  public showImageCropper: boolean = false;

  public uploadedFile: any;
  public externalFiles: any[];
  imageChangedEvent: any = '';
  croppedImage: any = '../../../../assets/images/lts.png';
  cropperPosition: CropperPosition = {
    x1: 100,
    y1: 100,
    x2: 200,
    y2: 200
  }
  constructor() { }

  ngOnInit(): void {
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.showImageCropperPopup = true;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.imageCropper.imageBase64;
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    this.showImageCropper = true;
    setTimeout(() => {
      this.cropperPosition = {
        x1: 100,
        y1: 100,
        x2: 200,
        y2: 200
      }
    });
  }
  cropperReady(dimensions: Dimensions) {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  closeImageCropper() {
    this.croppedImage = this.imageCropper.crop().base64;
    this.showImageCropperPopup = false;
  }
}
