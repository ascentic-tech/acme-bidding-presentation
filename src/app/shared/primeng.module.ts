import { NgModule } from '@angular/core';
import { MenuModule } from 'primeng/menu';
import { MegaMenuModule } from 'primeng/megamenu';
import { PanelModule } from 'primeng/panel';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DropdownModule } from 'primeng/dropdown';
import { RippleModule } from 'primeng/ripple';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TabViewModule } from 'primeng/tabview';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputMaskModule } from 'primeng/inputmask';
import { CalendarModule } from 'primeng/calendar';
import { ChipsModule } from 'primeng/chips';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { InputNumberModule } from 'primeng/inputnumber';
import { TooltipModule } from 'primeng/tooltip';
import { TableModule } from 'primeng/table';
import { FieldsetModule } from 'primeng/fieldset';
import { FocusTrapModule } from 'primeng/focustrap';
import { CardModule } from 'primeng/card';
import { PasswordModule } from 'primeng/password';
import { StepsModule } from 'primeng/steps';
import { FileUploadModule } from 'primeng/fileupload';
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { MenubarModule } from 'primeng/menubar';
import { DataViewModule } from 'primeng/dataview';
import { BadgeModule } from 'primeng/badge';
import { AvatarModule } from 'primeng/avatar';
import { AvatarGroupModule } from 'primeng/avatargroup';
import {SplitButtonModule} from 'primeng/splitbutton';
import {VirtualScrollerModule} from 'primeng/virtualscroller';
import { TagModule } from 'primeng/tag';
import {SkeletonModule} from 'primeng/skeleton';
import {GalleriaModule} from 'primeng/galleria';

@NgModule({
  imports: [
    MenuModule,
    PanelModule,
    MegaMenuModule,
    ToastModule,
    CheckboxModule,
    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    DropdownModule,
    RippleModule,
    InputTextareaModule,
    TabViewModule,
    MultiSelectModule,
    InputMaskModule,
    CalendarModule,
    ChipsModule,
    AutoCompleteModule,
    InputNumberModule,
    TooltipModule,
    TableModule,
    FieldsetModule,
    FocusTrapModule,
    CardModule,
    PasswordModule,
    StepsModule,
    FileUploadModule,
    DialogModule,
    AccordionModule,
    MenubarModule,
    DataViewModule,
    BadgeModule,
    AvatarModule,
    AvatarGroupModule,
    SplitButtonModule,
    VirtualScrollerModule,
    TagModule,
    SkeletonModule,
    GalleriaModule
  ],
  providers: [MessageService],
  exports: [
    MenuModule,
    PanelModule,
    MegaMenuModule,
    ToastModule,
    CheckboxModule,
    InputTextModule,
    ButtonModule,
    RadioButtonModule,
    DropdownModule,
    RippleModule,
    InputTextareaModule,
    TabViewModule,
    MultiSelectModule,
    InputMaskModule,
    CalendarModule,
    ChipsModule,
    AutoCompleteModule,
    InputNumberModule,
    TooltipModule,
    TableModule,
    FieldsetModule,
    FocusTrapModule,
    CardModule,
    PasswordModule,
    StepsModule,
    FileUploadModule,
    DialogModule,
    AccordionModule,
    MenubarModule,
    DataViewModule,
    BadgeModule,
    AvatarModule,
    AvatarGroupModule,
    SplitButtonModule,
    VirtualScrollerModule,
    TagModule,
    SkeletonModule,
    GalleriaModule
  ]
})
export class PrimengModule { }
