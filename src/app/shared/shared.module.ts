import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { PrimengModule } from './primeng.module';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { ImageUploaderComponent } from './components/image-uploader/image-uploader.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { CountdownTimerComponent } from './components/countdown-timer/countdown-timer.component';
@NgModule({
  declarations: [
    TopBarComponent,
    ButtonsComponent,
    ImageUploaderComponent,
    CountdownTimerComponent],
  imports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    ImageCropperModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    PrimengModule,
    TopBarComponent,
    ButtonsComponent,
    ImageUploaderComponent,
    CountdownTimerComponent]
})
export class SharedModule { }
